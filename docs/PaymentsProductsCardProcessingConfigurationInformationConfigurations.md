# PaymentsProductsCardProcessingConfigurationInformationConfigurations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**common** | [**PaymentsProductsCardProcessingConfigurationInformationConfigurationsCommon**](PaymentsProductsCardProcessingConfigurationInformationConfigurationsCommon.md) |  | [optional] 
**features** | [**PaymentsProductsCardProcessingConfigurationInformationConfigurationsFeatures**](PaymentsProductsCardProcessingConfigurationInformationConfigurationsFeatures.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


