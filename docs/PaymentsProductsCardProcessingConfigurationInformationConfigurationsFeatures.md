# PaymentsProductsCardProcessingConfigurationInformationConfigurationsFeatures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_not_present** | [**PaymentsProductsCardProcessingConfigurationInformationConfigurationsFeaturesCardNotPresent**](PaymentsProductsCardProcessingConfigurationInformationConfigurationsFeaturesCardNotPresent.md) |  | [optional] 
**card_present** | [**PaymentsProductsCardProcessingConfigurationInformationConfigurationsFeaturesCardPresent**](PaymentsProductsCardProcessingConfigurationInformationConfigurationsFeaturesCardPresent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


