# PaymentsProductsECheckConfigurationInformationConfigurations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**common** | [**PaymentsProductsECheckConfigurationInformationConfigurationsCommon**](PaymentsProductsECheckConfigurationInformationConfigurationsCommon.md) |  | [optional] 
**underwriting** | [**PaymentsProductsECheckConfigurationInformationConfigurationsUnderwriting**](PaymentsProductsECheckConfigurationInformationConfigurationsUnderwriting.md) |  | [optional] 
**features** | [**PaymentsProductsECheckConfigurationInformationConfigurationsFeatures**](PaymentsProductsECheckConfigurationInformationConfigurationsFeatures.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


