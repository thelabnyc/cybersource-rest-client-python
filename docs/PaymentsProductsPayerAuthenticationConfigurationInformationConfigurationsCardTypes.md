# PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**verified_by_visa** | [**PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa**](PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa.md) |  | [optional] 
**master_card_secure_code** | [**PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa**](PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa.md) |  | [optional] 
**amex_safe_key** | [**PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa**](PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa.md) |  | [optional] 
**j_cbj_secure** | [**PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesJCBJSecure**](PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesJCBJSecure.md) |  | [optional] 
**diners_club_international_protect_buy** | [**PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa**](PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa.md) |  | [optional] 
**elo** | [**PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa**](PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa.md) |  | [optional] 
**upi** | [**PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa**](PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesVerifiedByVisa.md) |  | [optional] 
**cb** | [**PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesCB**](PaymentsProductsPayerAuthenticationConfigurationInformationConfigurationsCardTypesCB.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


