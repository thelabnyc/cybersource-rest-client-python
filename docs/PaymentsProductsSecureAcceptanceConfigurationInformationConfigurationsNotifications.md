# PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsNotifications

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchant_notifications** | [**PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsNotificationsMerchantNotifications**](PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsNotificationsMerchantNotifications.md) |  | [optional] 
**customer_notifications** | [**PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsNotificationsCustomerNotifications**](PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsNotificationsCustomerNotifications.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


