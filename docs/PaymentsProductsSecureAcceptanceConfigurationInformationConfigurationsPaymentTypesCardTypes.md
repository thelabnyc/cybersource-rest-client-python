# PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsPaymentTypesCardTypes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discover** | [**PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsPaymentTypesCardTypesDiscover**](PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsPaymentTypesCardTypesDiscover.md) |  | [optional] 
**amex** | [**PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsPaymentTypesCardTypesDiscover**](PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsPaymentTypesCardTypesDiscover.md) |  | [optional] 
**master_card** | [**PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsPaymentTypesCardTypesDiscover**](PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsPaymentTypesCardTypesDiscover.md) |  | [optional] 
**visa** | [**PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsPaymentTypesCardTypesDiscover**](PaymentsProductsSecureAcceptanceConfigurationInformationConfigurationsPaymentTypesCardTypesDiscover.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


