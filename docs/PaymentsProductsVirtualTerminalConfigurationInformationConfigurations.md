# PaymentsProductsVirtualTerminalConfigurationInformationConfigurations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_not_present** | [**PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresent**](PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresent.md) |  | [optional] 
**card_present** | [**PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresent**](PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


