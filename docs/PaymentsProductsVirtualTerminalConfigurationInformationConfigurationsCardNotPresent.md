# PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**global_payment_information** | [**PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentGlobalPaymentInformation**](PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentGlobalPaymentInformation.md) |  | [optional] 
**receipt_information** | [**PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentReceiptInformation**](PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentReceiptInformation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


