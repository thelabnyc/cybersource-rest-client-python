# PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentReceiptInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentReceiptInformationHeader**](PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentReceiptInformationHeader.md) |  | [optional] 
**order_information** | [**PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentReceiptInformationOrderInformation**](PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentReceiptInformationOrderInformation.md) |  | [optional] 
**email_receipt** | [**PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentReceiptInformationEmailReceipt**](PaymentsProductsVirtualTerminalConfigurationInformationConfigurationsCardNotPresentReceiptInformationEmailReceipt.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


