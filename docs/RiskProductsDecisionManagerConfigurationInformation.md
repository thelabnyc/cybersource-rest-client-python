# RiskProductsDecisionManagerConfigurationInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**template_id** | **str** |  | [optional] 
**configurations** | [**RiskProductsDecisionManagerConfigurationInformationConfigurations**](RiskProductsDecisionManagerConfigurationInformationConfigurations.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


