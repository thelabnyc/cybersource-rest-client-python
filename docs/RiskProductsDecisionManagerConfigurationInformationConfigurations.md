# RiskProductsDecisionManagerConfigurationInformationConfigurations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processing_options** | [**RiskProductsDecisionManagerConfigurationInformationConfigurationsProcessingOptions**](RiskProductsDecisionManagerConfigurationInformationConfigurationsProcessingOptions.md) |  | [optional] 
**organization** | [**RiskProductsDecisionManagerConfigurationInformationConfigurationsOrganization**](RiskProductsDecisionManagerConfigurationInformationConfigurationsOrganization.md) |  | [optional] 
**portfolio_controls** | [**RiskProductsDecisionManagerConfigurationInformationConfigurationsPortfolioControls**](RiskProductsDecisionManagerConfigurationInformationConfigurationsPortfolioControls.md) |  | [optional] 
**thirdparty** | [**RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdparty**](RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdparty.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


