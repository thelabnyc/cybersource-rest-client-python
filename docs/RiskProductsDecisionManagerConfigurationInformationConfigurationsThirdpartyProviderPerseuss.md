# RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderPerseuss

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** |  | [optional] 
**enable_real_time** | **bool** |  | [optional] 
**credentials** | [**RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderAccurintCredentials**](RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderAccurintCredentials.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


