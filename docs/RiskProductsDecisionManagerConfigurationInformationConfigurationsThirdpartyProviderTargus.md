# RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderTargus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** |  | [optional] 
**use_cybs_credentials** | **bool** |  | [optional] 
**credentials** | [**RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderTargusCredentials**](RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderTargusCredentials.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


