# coding: utf-8

"""
    CyberSource Merged Spec

    All CyberSource API specs merged together. These are available at https://developer.cybersource.com/api/reference/api-reference.html

    OpenAPI spec version: 0.0.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import CyberSource
from CyberSource.rest import ApiException
from CyberSource.models.pts_v2_credits_post201_response_1_installment_information import PtsV2CreditsPost201Response1InstallmentInformation


class TestPtsV2CreditsPost201Response1InstallmentInformation(unittest.TestCase):
    """ PtsV2CreditsPost201Response1InstallmentInformation unit test stubs """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPtsV2CreditsPost201Response1InstallmentInformation(self):
        """
        Test PtsV2CreditsPost201Response1InstallmentInformation
        """
        # FIXME: construct object with mandatory attributes with example values
        #model = CyberSource.models.pts_v2_credits_post201_response_1_installment_information.PtsV2CreditsPost201Response1InstallmentInformation()
        pass


if __name__ == '__main__':
    unittest.main()
