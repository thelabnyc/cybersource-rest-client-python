# coding: utf-8

"""
    CyberSource Merged Spec

    All CyberSource API specs merged together. These are available at https://developer.cybersource.com/api/reference/api-reference.html

    OpenAPI spec version: 0.0.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import CyberSource
from CyberSource.rest import ApiException
from CyberSource.models.risk_products_decision_manager_configuration_information_configurations_thirdparty_provider_accurint import RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderAccurint


class TestRiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderAccurint(unittest.TestCase):
    """ RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderAccurint unit test stubs """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testRiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderAccurint(self):
        """
        Test RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderAccurint
        """
        # FIXME: construct object with mandatory attributes with example values
        #model = CyberSource.models.risk_products_decision_manager_configuration_information_configurations_thirdparty_provider_accurint.RiskProductsDecisionManagerConfigurationInformationConfigurationsThirdpartyProviderAccurint()
        pass


if __name__ == '__main__':
    unittest.main()
